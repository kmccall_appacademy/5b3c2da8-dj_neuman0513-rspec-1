def echo (string)
  return string
end

def shout (string)
  return string.upcase
end

def repeat(string, n=2)
  ([string] * n).join ' '
end

def start_of_word (string, n)
  string[0...n]
end

def first_word (string)
  words  = string.split
  words.first
end

def titleize(string)
  words = string.split(' ')
  small_words = ["and", "over", "the"]

  title = words.each do |word|
    word.capitalize! unless small_words.include?(word)
  end
  title[0].capitalize!
  title.join(' ')

end

def ftoc(n)
      ((n - 32) * 5) / 9
end

def ctof(n)
  ((n.to_f * 9) / 5 ) + 32
end
